#-------------------------------------------------
#
# Project created by QtCreator 2014-12-29T01:07:36
#
#-------------------------------------------------

QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Chat
TEMPLATE = app


SOURCES += main.cpp\
    chat.cpp

HEADERS  += \
    chat.h

FORMS    +=

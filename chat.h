#ifndef BROADCASTSENDER
#define BROADCASTSENDER

 #include <QWidget>
 #include <QTextEdit>
 #include <QLineEdit>
 #include <QBoxLayout>
 #include <QListView>
 #include <set>
 #include <QMainWindow>
 #include <QLabel>
 #include <QPushButton>
 #include <QListWidget>

 class QDialogButtonBox;
 class QLabel;
 class QPushButton;
 class QTimer;
 class QUdpSocket;

 class Chat : public QMainWindow
 {
     Q_OBJECT

 public:
     Chat(QWidget *parent = 0); //constructor from QWidget pointer which guarantees that there is nothing on its address

 private slots:
     void sendMessage(); // this method sends message from text field using UDP broadcast
     void processPendingDatagrams();// this method receives pending datagrams and processes them
     void insertNickname();// this method is called to add nickname in nicknames list if there is no such nickname yet

 private:
     std::set<QString> nicknames;

     QUdpSocket *sendSocket;
     QUdpSocket *receiveSocket;

     QVBoxLayout *mainLayout;

     QHBoxLayout *nicknameLayout;
     QLabel *nicknameLabel;
     QLineEdit *nicknameInput;

     QHBoxLayout *chatLayout;
     QTextEdit *chatHistory;
     QListWidget *nicknamesList;

     QHBoxLayout *inputLayout;
     QLineEdit *userInput;
     QPushButton *sendButton;

     void addNickname(QString);
 };

#endif // BROADCASTSENDER


 #include <QtGui>
 #include <QtNetwork>

 #include "chat.h"

 Chat::Chat(QWidget *parent)
     : QMainWindow(parent){
     sendSocket = new QUdpSocket(this);
     receiveSocket = new QUdpSocket(this);
     if (!receiveSocket->bind(65333, QUdpSocket::ShareAddress))
         return;
     connect(receiveSocket, SIGNAL(readyRead()), this, SLOT(processPendingDatagrams()));

     nicknameLabel = new QLabel("Nickname: ");
     nicknameInput = new QLineEdit;

     chatHistory = new QTextEdit;
     chatHistory->setReadOnly(true);

     nicknamesList = new QListWidget;
     connect(nicknamesList, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(insertNickname()));

     userInput = new QLineEdit;
     connect(userInput, SIGNAL(returnPressed()), this, SLOT(sendMessage()));
     sendButton = new QPushButton("Send");
     connect(sendButton, SIGNAL(clicked()), this, SLOT(sendMessage()));

     nicknameLayout = new QHBoxLayout;
     nicknameLayout->addWidget(nicknameLabel);
     nicknameLayout->addWidget(nicknameInput);

     chatLayout = new QHBoxLayout;
     chatLayout->addWidget(chatHistory);
     chatLayout->addWidget(nicknamesList);

     inputLayout = new QHBoxLayout;
     inputLayout->addWidget(userInput);
     inputLayout->addWidget(sendButton);

     mainLayout = new QVBoxLayout;
     mainLayout->addItem(nicknameLayout);
     mainLayout->addItem(chatLayout);
     mainLayout->addItem(inputLayout);

     QWidget *w = new QWidget;
     w->setLayout(mainLayout);

     setCentralWidget(w);
     setWindowTitle(tr("Chat"));
 }


 void Chat::sendMessage()
 {
     QTextCodec *codec = QTextCodec::codecForName("UTF-8");
     QString message = nicknameInput->text() + ": " + userInput->text();
     userInput->clear();
     QByteArray datagram = codec->fromUnicode(message);
     sendSocket->writeDatagram(datagram.data(), datagram.size(),
                              QHostAddress::Broadcast, 65333);
 }

 void Chat::insertNickname() {
     QString nickname = nicknamesList->selectedItems()[0]->text();
     if (nickname != nicknameInput->text()) {
        userInput->setText(nickname + ", ");
        userInput->setFocus();
     }
 }

 void Chat::addNickname(QString nickname) {
    nicknamesList->addItem(nickname);
 }

 void Chat::processPendingDatagrams()
 {
     while (receiveSocket->hasPendingDatagrams()) {
         QByteArray datagram;
         datagram.resize(receiveSocket->pendingDatagramSize());
         receiveSocket->readDatagram(datagram.data(), datagram.size());
         QTextCodec *codec = QTextCodec::codecForName("UTF-8");
         QString message = codec->fromUnicode(datagram);
         QStringList parts = message.split(":");
         chatHistory->append(message);
         if (nicknames.find(parts[0]) == nicknames.end()) {
             nicknames.insert(parts[0]);
             addNickname(parts[0]);
         }
     }
 }
